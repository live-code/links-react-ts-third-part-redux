import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { combineReducers, configureStore, ThunkAction, Action, getDefaultMiddleware } from '@reduxjs/toolkit';

import { CounterPage } from './features/counter/CounterPage';
import { CatalogPage } from './features/catalog/CatalogPage';
import { UsersPage } from './features/users/UsersPage';
import { Navbar } from './core/components/Navbar';
import { HomePage } from './features/home/HomePage';
import { counterReducer } from './features/counter/store/counter.reducer';
import { usersStore } from './features/users/store/users.store';
import { catalogReducer } from './features/catalog/store/reducers';
import { logger } from './core/middlewares/logger.middleware';

const rootReducer = combineReducers({
  users: usersStore.reducer,
  // users: usersReducer,
  counter: counterReducer,
  catalog: catalogReducer
});

export type RootState = ReturnType<typeof rootReducer>

export const store = configureStore({
  reducer: rootReducer,
  devTools: process.env.NODE_ENV !== 'production',
  middleware: [...getDefaultMiddleware(), logger]
});

export type AppThunk = ThunkAction<void, RootState, null, Action<string>>;


const App: React.FC = () => {
  return (
    <BrowserRouter>
      <Navbar />

      <Switch>
        <Route path="/" exact>
          <HomePage />
        </Route>
        <Route path="/counter">
          <CounterPage />
        </Route>
        <Route path="/users">
          <UsersPage />
        </Route>
        <Route path="/catalog">
          <CatalogPage />
        </Route>
      </Switch>
    </BrowserRouter>
  );
}

export default App;
