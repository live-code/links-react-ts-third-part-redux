import { Middleware } from "@reduxjs/toolkit";

export const logger: Middleware = store => {
  return next => {
    return action => {
      console.log('current state', store.getState());
      console.log('dispatching', action)
      console.log('next', next)
      next(action);
      console.log('next state', store.getState())
    }
  }
}
