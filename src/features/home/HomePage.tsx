import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getProducts } from '../catalog/store/selectors/products.selectors';
import { loadProducts } from '../catalog/store/actions/products.actions';

export const HomePage: React.FC = () => {
  const products = useSelector(getProducts);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(loadProducts());
  }, []);

  return <div>
    Home {products.length}
  </div>
};
