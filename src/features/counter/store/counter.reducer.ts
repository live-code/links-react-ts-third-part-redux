import { createReducer } from '@reduxjs/toolkit';
import { decrement, INCREMENT, reset } from './counter.actions';

export const counterReducer = createReducer(0, {
  [INCREMENT]: (state, action) => state + action.payload,
  [decrement.type]: (state, action) => state - action.payload,
  [reset.type]: () => 0,
})


/*
// OLD SCHOOL
import { DECREMENT, INCREMENT } from './counter.actions';

export function counterReducer(state = 0, action: any) {
  switch (action.type) {
    case INCREMENT:
      return state + action.payload;
    case DECREMENT:
      return state - action.payload;
    default:
      return state;
  }
}
*/
