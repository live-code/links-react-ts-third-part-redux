import { RootState } from '../../../App';

export const getCounter = (state: RootState) => state.counter;
export const getDozens = (state: RootState) => Math.floor(state.counter / 12);
