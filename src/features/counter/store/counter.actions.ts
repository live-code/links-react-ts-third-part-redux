import { createAction } from '@reduxjs/toolkit';

export const INCREMENT = 'INCREMENT';
export const DECREMENT = 'DECREMENT';
export const RESET = 'RESET';
/*
export function increment(value: number) {
  return { type: INCREMENT, payload: value }
}

export function decrement(value: number) {
  return { type: DECREMENT, payload: value }
}
*/


export const increment = createAction<number>(INCREMENT);
export const decrement = createAction<number>(DECREMENT);
export const reset = createAction<void>(RESET);
