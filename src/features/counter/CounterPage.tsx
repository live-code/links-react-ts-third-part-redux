import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { decrement, increment, reset } from './store/counter.actions';
import { getCounter, getDozens } from './store/counter.selectors';

export const CounterPage: React.FC = () => {
  const dispatch = useDispatch();
  const counter =  useSelector(getCounter);
  const dozens =  useSelector(getDozens);

  return <div>
    <h1>Counter: {counter}</h1>
    <h1>Dozens: {dozens}</h1>
    <button onClick={() => dispatch(increment(10))}>+</button>
    <button onClick={() => dispatch(decrement(5))}>-</button>
    <button onClick={() => dispatch(reset())}>RESET</button>
  </div>
};
