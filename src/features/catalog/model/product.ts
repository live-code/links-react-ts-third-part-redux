export interface Product {
  id: number;
  title: string;
  price: number;
  visibility: boolean;
  categoryID: number
}

export type ProductForm = Omit<Product, 'id' | 'visibility'>
