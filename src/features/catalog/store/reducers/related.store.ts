import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { Product } from '../../model/product';

export const relatedStore = createSlice({
  name: 'related',
  initialState: {
    list: [] as Product[],
    error: false,
    pending: false
  },
  reducers: {
    loadRelatedStart(state, action: PayloadAction<void>) {
      return {
        ...state,
        pending: true
      }
    },
    loadRelatedSuccess(state, action: PayloadAction<Product[]>) {
      return {
        error: false,
        pending: false,
        list: [...action.payload],
      };
    },
    loadRelatedFailed(state) {
      return {
        ...state,
        pending: false,
        error: true
      };
    }
  }
})

export const {
  loadRelatedSuccess,
  loadRelatedFailed,
  loadRelatedStart
} = relatedStore.actions;
