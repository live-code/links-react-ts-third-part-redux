import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { Product } from '../../model/product';

export const productsStore = createSlice({
  name: 'products',
  initialState: [] as Product[],
  reducers: {
    loadProductsSuccess(state, action: PayloadAction<Product[]>) {
      return [...action.payload];
    },
    addProductSuccess(state, action: PayloadAction<Product>) {
      return [...state, action.payload]
    },
    deleteProductSuccess(state, action: PayloadAction<number>) {
      return state.filter(p => p.id !== action.payload)
    },
  }
})

export const {
  loadProductsSuccess,
  addProductSuccess,
  deleteProductSuccess
} = productsStore.actions;
