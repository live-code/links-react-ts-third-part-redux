import { createSlice } from '@reduxjs/toolkit';

export const categoriesStore = createSlice({
  name: 'related',
  initialState: [
    { id: 1, name: 'Latticini'},
    { id: 2, name: 'Cioccolata'},
  ],
  reducers: {

  }
});
