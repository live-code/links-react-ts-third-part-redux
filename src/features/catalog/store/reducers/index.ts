import { combineReducers } from '@reduxjs/toolkit';
import { productsStore } from './products.store';
import { relatedStore } from './related.store';
import { categoriesStore } from './caategories.store';

export const catalogReducer = combineReducers({
  products: productsStore.reducer,
  related: relatedStore.reducer,
  categories: categoriesStore.reducer
})
