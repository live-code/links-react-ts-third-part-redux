import { RootState } from '../../../../App';

export const getCategories = (state: RootState) => state.catalog.categories;

