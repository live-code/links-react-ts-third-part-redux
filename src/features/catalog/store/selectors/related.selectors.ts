import { RootState } from '../../../../App';

export const getRelated = (state: RootState) => state.catalog.related.list;
export const getRelatedError = (state: RootState) => state.catalog.related.error;
export const getRelatedPending = (state: RootState) => state.catalog.related.pending;

