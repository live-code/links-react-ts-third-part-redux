import { RootState } from '../../../../App';
import { Product } from '../../model/product';

export const getProducts = (state: RootState) => state.catalog.products;

export const getProductsByCat = (id: number) => (state: RootState) => {
  return id !== -1?
    state.catalog.products.filter(p => p.categoryID === id) :
    state.catalog.products
}

export const getTotal = (state: RootState) => {
  return state.catalog.products.reduce((acc: number, curr: Product) => acc + curr.price, 0)
}



/*

export const getTotal = (state: RootState) => {
  let total = 0;
  for (let i = 0; i < state.catalog.length; i++) {
    total += state.catalog[i].price;
  }
  return total;
}
*/
