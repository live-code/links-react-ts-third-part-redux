import { AppThunk } from '../../../../App';
import axios, {AxiosResponse} from 'axios';
import { loadRelatedSuccess, loadRelatedFailed, loadRelatedStart } from '../reducers/related.store';
import { Product } from '../../model/product';

// export const loadRelatedAction = createAction<void>('related/loadRelated')

export const loadRelated = (): AppThunk => (dispatch)  => {
  dispatch(loadRelatedStart())
  setTimeout(() => {
    axios.get('http://localhost:3001/related')
      .then((res: AxiosResponse<Product[]>) => dispatch(loadRelatedSuccess(res.data)))
      .catch(err => dispatch(loadRelatedFailed()))
  }, 1000)

};
