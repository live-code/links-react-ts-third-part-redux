import { AppThunk } from '../../../../App';
import axios, {AxiosResponse} from 'axios';
import { loadProductsSuccess, deleteProductSuccess, addProductSuccess } from '../reducers/products.store';
import { Product } from '../../model/product';
import { createAction } from '@reduxjs/toolkit';

export const addProductAction = createAction<Omit<Product, 'id' | 'visibility'>>('products/add');
export const deleteProductAction = createAction<number>('products/delete');

export const loadProducts = (): AppThunk => (dispatch)  => {
  axios.get('http://localhost:3001/products')
    .then((res: AxiosResponse<Product[]>) => dispatch(loadProductsSuccess(res.data)));
};

export const deleteProduct = (id: number): AppThunk => dispatch  => {
  dispatch(deleteProductAction(id))
  axios.delete('http://localhost:3001/products/' + id)
    .then(() => dispatch(deleteProductSuccess(id)));
};

export const addProduct = (product: Omit<Product, 'id' | 'visibility'>): AppThunk => async dispatch  => {
  dispatch(addProductAction(product));
  const p: Omit<Product, 'id'> = { ...product, visibility: false}
  // const p: Product = { ...product, visibility: false} as Product;  // as alternative
  const response: AxiosResponse<Product> = await axios.post('http://localhost:3001/products/', p)
  dispatch(addProductSuccess(response.data));
};
