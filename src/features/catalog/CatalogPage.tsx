import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { addProduct, deleteProduct, loadProducts } from './store/actions/products.actions';
import { getProductsByCat, getTotal } from './store/selectors/products.selectors';
import { loadRelated } from './store/actions/related.actions';
import { getRelated, getRelatedError, getRelatedPending } from './store/selectors/related.selectors';
import { getCategories } from './store/selectors/categories.selectors';

export const CatalogPage: React.FC = () => {
  const [filterCategoryID, setFilterCategoryID] = useState<number>(-1);


  const dispatch = useDispatch();
  // const products = useSelector(getProducts);
  const products = useSelector(getProductsByCat(filterCategoryID));
  const related = useSelector(getRelated);
  const categories = useSelector(getCategories);
  const relatedError = useSelector(getRelatedError);
  const relatedPending = useSelector(getRelatedPending);
  const total = useSelector(getTotal)


  useEffect(() => {
    dispatch(loadProducts());
    dispatch(loadRelated());
  }, []);

  function addProductHandler() {
    dispatch(addProduct({
      title: 'product ' + Date.now().toString(),
      price: 10,
      categoryID: 1
    }))
  }

  function onSelectHandler(e: React.FormEvent<HTMLSelectElement>) {
    console.log(e.currentTarget.value)
    setFilterCategoryID(+e.currentTarget.value)
  }

  return <div>
    { relatedError && <div className="alert alert-danger">errore related</div> }
    { relatedPending && <i className="fa fa-spinner fa-spin fa-3x fa-fw"></i> }

    Filter by:
    <button onClick={() => setFilterCategoryID(-1)} >Tutto</button>
    <button onClick={() => setFilterCategoryID(1)}>Latticini</button>
    <button onClick={() => setFilterCategoryID(2)}>Cioccio</button>

    <select onChange={onSelectHandler} value={filterCategoryID}>
      <option value={-1}>Tutti</option>
      {
        categories.map(c => {
          return <option key={c.id} value={c.id}>{c.name}</option>
        })
      }
    </select>

    <hr/>
    <button onClick={addProductHandler}>ADD PRODUCT</button>

    {
      products.map(p => {
        return <li key={p.id}>
          {p.title } - {p.price}
          <i
            className="fa fa-trash"
            onClick={() => dispatch(deleteProduct(p.id))}/>
        </li>
      })

    }
    <hr/>
    € {total}
    <hr/>


    Total Related: {related.length} products


  </div>
};
