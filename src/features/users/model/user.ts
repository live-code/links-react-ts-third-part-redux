export interface User {
  id: number;
  name: string;
  isAdmin: boolean;
  isEnabled: boolean
}
