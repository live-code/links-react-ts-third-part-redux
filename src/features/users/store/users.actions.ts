import { User } from '../model/user';
import { createAction } from '@reduxjs/toolkit';

export enum UsersActionsTypes {
  'USER_ADD' = 'users/add',
  'DELETE_USER' = 'users/delete',
  'SET_AS_ADMIN' = 'users/setAsAdmin',
  'ENABLE_USER' = 'users/enableUser',
}

interface AddUserAction {
  type: UsersActionsTypes.USER_ADD;
  payload: User
}

interface DeleteUserAction {
  type: UsersActionsTypes.DELETE_USER;
  payload: number
}

interface SetAsAdminAction {
  type: UsersActionsTypes.SET_AS_ADMIN
  payload: User
}

interface EnableUserAction {
  type: UsersActionsTypes.ENABLE_USER
  payload: number
}

export const addUser = createAction<User>(UsersActionsTypes.USER_ADD);
export const deleteUser = createAction<number>(UsersActionsTypes.DELETE_USER);
export const setAsAdmin = createAction<User>(UsersActionsTypes.SET_AS_ADMIN);
export const enableUser = createAction<number>(UsersActionsTypes.ENABLE_USER);

export type UsersActions =
  AddUserAction |
  DeleteUserAction |
  SetAsAdminAction |
  EnableUserAction;


/*
SOLUTION 1
import { createAction } from '@reduxjs/toolkit';
import { User } from '../model/user';

export const addUser = createAction<User>('users/add');
export const deleteUser = createAction<number>('users/delete');
export const setAsAdmin = createAction<User>('users/setAsAdmin');
export const enableUser = createAction<number>('users/enableUser');
*/
