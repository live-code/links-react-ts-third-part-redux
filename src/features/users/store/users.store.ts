import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { User } from '../model/user';

const INITIAL_STATE: User[] = [];

export const usersStore = createSlice({
  name: 'customers',
  initialState: INITIAL_STATE,
  reducers: {
    addUser(state, action: PayloadAction<User>) {
      state.push(action.payload);
      // return [...state, action.payload];
    },
    deleteUser(state, action: PayloadAction<number>) {
      const index =  state.findIndex(u => u.id === action.payload);
      state.splice(index, 1);
      // return state.filter(u => u.id !== action.payload);
    },
    setAsAdmin(state, action: PayloadAction<User>) {
      const user =  state.find(u => u.id === action.payload.id)
      if (user) {
        user.isAdmin = !user.isAdmin;
      }
      /* return state.map(u => {
         return u.id === action.payload.id ? {...u, isAdmin: !u.isAdmin } : u;
       });*/
    },
    enableUser(state, action: PayloadAction<number>) {
      return state.map(u => {
        return u.id === action.payload ? {...u, isEnabled: !u.isEnabled } : u;
      });
    },

  }
})

export const { addUser, enableUser, deleteUser, setAsAdmin } = usersStore.actions
