import { User } from '../model/user';
import { UsersActions, UsersActionsTypes } from './users.actions';

// SOLUTION 2: reducer with type guard
const INITIAL_STATE: User[] = [];

export function usersReducer(state = INITIAL_STATE, action: UsersActions) {

  switch (action.type) {
    case UsersActionsTypes.USER_ADD:
      return [...state, action.payload];

    case UsersActionsTypes.DELETE_USER:
      return state.filter(u => u.id !== action.payload);

    case UsersActionsTypes.SET_AS_ADMIN:
      return state.map(u => {
        return u.id === action.payload.id ? {...u, isAdmin: !u.isAdmin } : u;
      });

    case UsersActionsTypes.ENABLE_USER:
      return state.map(u => {
        return u.id === action.payload ? {...u, isEnabled: !u.isEnabled } : u;
      });

    default:
      return state;
  }

}


/*
SOLUTION 1: createReducer (typed actions with PayloadAction)

import { User } from '../model/user';
import { createReducer, PayloadAction } from '@reduxjs/toolkit';
import { addUser, deleteUser, enableUser, setAsAdmin } from './users.actions';

const INITIAL_STATE: User[] = [];

export const usersReducer = createReducer(INITIAL_STATE, {
  [addUser.type]: (state, action: PayloadAction<User>) => {
    return [...state, action.payload];
  },
  [deleteUser.type]: (state, action: PayloadAction<number>) => {
    return state.filter(u => u.id !== action.payload);
  },
  [setAsAdmin.type]: (state, action: PayloadAction<User>) => {
    return state.map(u => {
      return u.id === action.payload.id ? {...u, isAdmin: !u.isAdmin } : u;
    })
  },
  [enableUser.type]: (state, action: PayloadAction<number>) => {
    return state.map(u => {
      return u.id === action.payload ? {...u, isEnabled: !u.isEnabled } : u;
    })
  },
})
*/
