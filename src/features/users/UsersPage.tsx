import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../App';
import cn from 'classnames';
import { addUser, deleteUser, enableUser, setAsAdmin } from './store/users.store';


const getUsers = (state: RootState) => state.users;
const getAdmins = (state: RootState) => state.users.filter(u => u.isAdmin);

export const UsersPage: React.FC = () => {
  const users = useSelector(getUsers)
  const admins = useSelector(getAdmins);
  const dispatch = useDispatch();

  function addUserHandler() {
    const id = Date.now();
    dispatch(addUser({
      id,
      name: 'User ' + id,
      isAdmin: false,
      isEnabled: true
    }));
  }

  return <div>
    UsersPage: {users.length} Utenti
    <br/>
    Admins: {admins.length} Utenti

    <hr/>

    <button onClick={addUserHandler}>ADD USER</button>

    {
      users.map(u => {
        console.log(u.isAdmin)
        return (
          <li
            className="list-group-item"
            key={u.id}
            style={{
              background: u.isEnabled ? 'white' : 'grey'
            }}
          >
            <i
              className={cn(
                'fa',
                {'fa-check': u.isAdmin},
                {'fa-square-o': !u.isAdmin},
              )}
              onClick={() => dispatch(setAsAdmin(u))}
            />
            <span> {u.name}</span>

            <div className="pull-right">
              <i className="fa fa-eye" onClick={() => dispatch(enableUser(u.id))}/>
              <i className="fa fa-trash" onClick={() => dispatch(deleteUser(u.id))}></i>
            </div>
          </li>
        )
      })

    }
    <hr/>
  </div>
};
